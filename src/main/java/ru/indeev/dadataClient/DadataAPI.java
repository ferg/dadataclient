package ru.indeev.dadataClient;

import org.springframework.cloud.openfeign.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import ru.indeev.dadataClient.json_views.*;

@FeignClient(url = "https://dadata.ru/api/v2", name = "dadata-client")
interface DadataAPI {

    @PostMapping("/clean/address")
    ClearAddress[] cleanAddress(@RequestHeader("Authorization") String authorization,
                                @RequestHeader("X-Secret") String secret,
                                @RequestBody String requestAddress);
}
