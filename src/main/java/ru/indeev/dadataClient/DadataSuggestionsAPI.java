package ru.indeev.dadataClient;

import feign.*;
import org.springframework.cloud.openfeign.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import ru.indeev.dadataClient.json_views.*;

@Service
@FeignClient(url = "https://suggestions.dadata.ru/suggestions/api/4_1/rs", name = "dadataSuggestions-client")
interface DadataSuggestionsAPI {


    @PostMapping("/suggest/address")
    Suggestions getAddress(@RequestHeader("Authorization") String authorization,
                           AddressQuery query);

    @PostMapping("/findById/fias")
    Suggestions getFias(@RequestHeader("Authorization") String authorization,
                           AddressQuery query);
}
