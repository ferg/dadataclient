package ru.indeev.dadataClient;

import lombok.*;
import org.springframework.boot.context.properties.*;

@Data
@ConfigurationProperties(prefix = "dadata")
class DadataClientProperties {

    private String token;
    private String secret;

}


