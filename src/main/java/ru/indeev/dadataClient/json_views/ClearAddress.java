package ru.indeev.dadataClient.json_views;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClearAddress {

    private String source;

    private String result;

    private String country;

    private String region;

    private String area;

    private String city;

    @JsonProperty("street_with_type")
    private String streetWithType;

    private String house;

    @JsonProperty("house_type")
    private String houseType;

    private String block;

    @JsonProperty("block_type")
    private String blockType;

    private String flat;

    @JsonProperty("geo_lat")
    private String geoLat;

    @JsonProperty("geo_lon")
    private String geoLon;

    @JsonProperty("beltway_hit")
    private String beltwayHit;

}

