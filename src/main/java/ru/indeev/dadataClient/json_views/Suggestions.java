package ru.indeev.dadataClient.json_views;

import lombok.*;

import java.util.*;

@Data
@NoArgsConstructor
public class Suggestions {

    private ArrayList<Suggestion> suggestions;
}
