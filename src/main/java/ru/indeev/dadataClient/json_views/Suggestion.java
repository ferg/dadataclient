package ru.indeev.dadataClient.json_views;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Data
@NoArgsConstructor
public class Suggestion {

    private String value;

    @JsonProperty("unrestricted_value")
    private String unrestrictedValue;

    private SuggestionData data;
}
