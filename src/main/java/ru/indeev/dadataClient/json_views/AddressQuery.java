package ru.indeev.dadataClient.json_views;

import lombok.*;

@Data
@NoArgsConstructor
public class AddressQuery {

    private String query;
}
