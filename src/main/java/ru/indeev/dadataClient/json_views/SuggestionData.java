package ru.indeev.dadataClient.json_views;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SuggestionData {

    @JsonProperty("postal_code")
    private String postalCode;

    private String country;

    @JsonProperty("region_with_type")
    private String regionWithType;

    @JsonProperty("area_with_type")
    private String areaWithType;

    @JsonProperty("city_with_type")
    private String cityWithType;

    @JsonProperty("settlement_with_type")
    private String settlementWithType;

    @JsonProperty("street_with_type")
    private String streetWithType;

    private String house;

    @JsonProperty("house_type")
    private String houseType;

    private String block;

    @JsonProperty("block_type")
    private String blockType;

    private String flat;

    @JsonProperty("geo_lat")
    private String geoLat;

    @JsonProperty("geo_lon")
    private String geoLon;

    @JsonProperty("beltway_hit")
    private String beltwayHit;

    @JsonProperty("city_kladr_id")
    private String cityKladrId;

    @JsonProperty("settlement_kladr_id")
    private String settlementKladrId;

    @JsonProperty("kladr_id")
    private String kladrId;

    @JsonProperty("area_kladr_id")
    private String areaKladrId;

    @JsonProperty("city_fias_id")
    private String cityFiasId;

    @JsonProperty("settlement_fias_id")
    private String settlementFiasId;
}
