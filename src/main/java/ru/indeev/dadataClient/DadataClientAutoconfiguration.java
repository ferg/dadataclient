package ru.indeev.dadataClient;

import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.context.properties.*;
import org.springframework.cloud.openfeign.*;
import org.springframework.context.annotation.*;

@Configuration
@EnableConfigurationProperties(DadataClientProperties.class)
@EnableFeignClients
public class DadataClientAutoconfiguration {

    @Autowired
    private DadataClientProperties dadataClientProperties;

    @Autowired
    private  DadataSuggestionsAPI dadataSuggestionsAPI;


    @Bean
    public DadataService dadataService(){
        return new DadataService(dadataClientProperties.getToken(), dadataClientProperties.getSecret(), dadataSuggestionsAPI);
    }
}
