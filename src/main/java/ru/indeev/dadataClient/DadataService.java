package ru.indeev.dadataClient;

import ru.indeev.dadataClient.json_views.*;

import java.util.*;

public class DadataService {

    private  DadataSuggestionsAPI dadataSuggestionsAPI;

    private String TOKEN;

    private String SECRET;


    public DadataService(String TOKEN, String SECRET,DadataSuggestionsAPI dadataSuggestionsAPI ) {
        this.TOKEN = TOKEN;
        this.SECRET = SECRET;
        this.dadataSuggestionsAPI = dadataSuggestionsAPI;
    }

    public List<Suggestion> getAddress(String addressString){
        AddressQuery addressQuery = new AddressQuery();
        addressQuery.setQuery(addressString);
        Suggestions suggestions = dadataSuggestionsAPI.getAddress(TOKEN, addressQuery);
        return suggestions.getSuggestions();
    }

    public Suggestion getFirstAddress(String addressString) {

        AddressQuery addressQuery = new AddressQuery();
        addressQuery.setQuery(addressString);
        Suggestions suggestions = dadataSuggestionsAPI.getAddress(TOKEN, addressQuery);
        Suggestion suggestion = suggestions.getSuggestions().get(0);
        return suggestion;
    }

    public Suggestion getFias(String addressString){
        AddressQuery addressQuery = new AddressQuery();
        addressQuery.setQuery(addressString);
        Suggestions suggestions = dadataSuggestionsAPI.getFias(TOKEN, addressQuery);
        return suggestions.getSuggestions().get(0);
    }
}
